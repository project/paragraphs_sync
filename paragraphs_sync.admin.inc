<?php

/**
 * Adds the synchronize fields form.
 *
 * @param array $form
 *   The paragraphs_admin_bundle form
 */
function paragraphs_sync_admin_paragraphs_bundle_settings(&$form) {

  $form['paragraphs_sync'] = array(
    '#type'        => 'fieldset',
    '#tree'        => TRUE,
    '#title'       => t('Synchronize translations'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#description' => t('Select which fields to synchronize for all translations of this paragraphs bundle.'),
  );

  $form['paragraphs_sync']['title'] = array(
    '#prefix' => '<strong>',
    '#suffix' => '</strong>',
    '#markup' => t('Configurable fields'),
  );

  $form['paragraphs_sync']['fields'] = array(
    '#tree' => TRUE,
  );

  $entity_type = 'paragraphs_item';
  $bundle = $form['#paragraphs_bundle']->bundle;
  foreach (i18n_sync_options($entity_type, $bundle) as $field => $info) {
    $form['paragraphs_sync']['fields'][$field] = array(
      '#title'         => $info['title'],
      '#type'          => 'checkbox',
      '#default_value' => paragraphs_sync_is_enabled_field($entity_type, $bundle, $field),
      '#description'   => isset($info['description']) ? $info['description'] : '',
    );
  }

  $form['#submit'][] = 'paragraphs_sync_admin_paragraphs_bundle_settings_submit';
}

/**
 * Submit callback for paragraphs_admin_bundle_form.
 */
function paragraphs_sync_admin_paragraphs_bundle_settings_submit($form, $form_state) {
  $values = $form_state['values'];
  $fields = array();
  if (!empty($values['paragraphs_sync']['fields'])) {
    $fields = array_keys(array_filter($values['paragraphs_sync']['fields']));
  }
  paragraphs_sync_set_enabled_fields('paragraphs_item', $form['#paragraphs_bundle']->bundle, $fields);
}
